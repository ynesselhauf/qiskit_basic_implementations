from qiskit import QuantumCircuit, execute, BasicAer

backend = BasicAer.get_backend('qasm_simulator')

qc = QuantumCircuit(2, 2)

qc.reset(0)
qc.reset(1)

qc.h(0)
qc.cx(0, 1)

qc.measure_all()

print(qc)

job = execute(qc, backend, shots=1000)
result = job.result()
counts = result.get_counts(qc)

print(counts)