from qiskit import *
import matplotlib.pyplot as plt
import numpy as np

oracle = QuantumCircuit(2,name='oracle')
oracle.cz(0,1)
oracle.to_gate()
print(oracle)

backend = Aer.get_backend('statevector_simulator')
grover_circ = QuantumCircuit(2,2)
grover_circ.h([0,1])
grover_circ.append(oracle, [0,1])
print(grover_circ)

job = execute(grover_circ, backend)
result = job.result()

sv = result.get_statevector()
np.around(sv, 2)

reflection = QuantumCircuit(2,name='reflection')
reflection.h([0,1])
reflection.z([0,1])
reflection.cz(0,1)
reflection.h([0,1])
reflection.to_gate()
print(reflection)

backend = Aer.get_backend('qasm_simulator')
grover_circ = QuantumCircuit(2,2)
grover_circ.h([0,1])
grover_circ.append(oracle, [0,1])
grover_circ.append(reflection,[0,1])
grover_circ.measure([0,1], [0,1])

print(grover_circ)

job = execute(grover_circ, backend, shots=1)
result = job.result()
counts = result.get_counts()
print(counts)